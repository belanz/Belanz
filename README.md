# Belanz

Belanz is a simple bookkeeping application for macOS based on the [Buchhaltung](https://gitlab.com/Kai.Oezer/Buchhaltung) library.


## Belanz file format compared to Buchhaltung

While in Buchhaltung there are two separate file formats for ledgers and for reports,
the Belanz file contains both Buchhaltung ledger and Buchhaltung report data.

## Release Procedure

### Standard Releases

A release is prepared on a separate branch, called a release branch and named "release-A.B.x", where A and B are placeholders for the major and minor version numbers, respectively, of the version(s) to be released from that branch. A release is created from a tagged commit on a release branch, where the tag name matches the release version. That is, release A.B.0 is built from the commit tagged "A.B.0" on branch "release-A.B.x". 

### Preview (Beta) Releases

Preview releases can be built from any commit on any branch that is tagged "beta-A.B.C", where "A.B.C" denotes a previously unreleased version of the software.
