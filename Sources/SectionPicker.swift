// Copyright 2022-2025 Kai Oezer

import SwiftUI
import OrderedCollections

/// Manages the setup of a window for navigating between document sections.
///
/// Adds a view that displays the root view of the selected document section.
/// Adds a segmented picker to the toolbar's navigation controls area.
/// Basically acts like  a `TabView` whose tab picker button is placed in the toolbar.
struct SectionPicker : View
{
	/// Use this to specify which sections can be navigated to
	let sections : OrderedSet<BelanzDocumentSection>

	@SceneStorage("Document.SelectedSection") var selection : BelanzDocumentSection = .companyInfo

	let sectionViews : [AnyView]

	@resultBuilder struct SectionsBuilder
	{
		static func buildPartialBlock(first content: any View) -> [AnyView]
		{
			[AnyView(content)]
		}

		static func buildPartialBlock(accumulated: [AnyView], next: any View) -> [AnyView]
		{
			accumulated + [AnyView(next)]
		}
	}

	init(
		sections: OrderedSet<BelanzDocumentSection>,
		@SectionsBuilder content : () -> [AnyView]
	)
	{
		self.sections = sections
		self.sectionViews = content()
		precondition(!sectionViews.isEmpty)
		precondition(sections.count == sectionViews.count)
	}

	var body : some View
	{
		Group
		{
			if let viewIndex = sections.firstIndex(of: selection) {
				sectionViews[viewIndex]
			} else {
				EmptyView()
			}
		}
		.toolbar {
			ToolbarItemGroup(placement: .navigation) {
				_segmentedPicker
			}
		}
		.focusedSceneObject(SectionPickerInfo(sections: sections, selectedSection: $selection))
	}

	private var _segmentedPicker : some View
	{
		Picker("section-picker.title", selection: $selection)
		{
			ForEach(sections) { section in
				section.icon
					.help(section.description)
					.tag(section)
					.accessibilityIdentifier(section.identifier)
			}
		}
		.pickerStyle(.segmented)
		.labelsHidden()
		.accessibilityIdentifier("section-picker")
	}
}
