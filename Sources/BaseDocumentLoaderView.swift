// Copyright 2023-2024 Kai Oezer

import SwiftUI

struct BaseDocumentLoaderView : View
{
	@ObservedObject var loader : BaseDocumentLoader

	@FocusState var locationEntryIsFocused : Bool
	@Environment(\.colorScheme) var colorScheme : ColorScheme
	@Environment(\.bhErrorColor) var bhInvalidColor : Color
	@Environment(\.isEnabled) var isEnabled : Bool

	var body : some View
	{
		HStack
		{
			_filePathField
			Spacer().frame(width: 4)
			_fileBrowseButton
			Spacer().frame(width: 8)
			_fileReloadButton
		}
	}

	private var _filePathField : some View
	{
		TextField(tr("options.base_report.loader.placeholder"), text: _filePath)
			.textFieldStyle(.roundedBorder)
			.truncationMode(.head)
			.autocorrectionDisabled()
			.focused($locationEntryIsFocused)
		#if os(macOS)
			.focusable(false)
		#endif
			.foregroundColor(loader.resolvedPath == nil ? bhInvalidColor : .none)
			.help(loader.path ?? "")
	}

	private var _fileBrowseButton : some View
	{
		FileOpenButton(fileTypes: [.belanz, .buchhaltungReport]) { url in
			if BelanzSettings.alwaysStoreRelativePathToBaseDocument,
			 let url,
			 let relativePath = loader.documentRelativePath(to: url) {
				loader.path = relativePath
			} else {
				loader.path = url?.path
			}
			Task { await loader.reload() }
		}
		.foregroundColor(isEnabled ? .accentColor : Color.gray)
		.help(tr("options.base_report.loader.browse.help"))
	}

	@ViewBuilder
	private var _fileReloadButton : some View
	{
		let buttonIsDisabled = !isEnabled || (loader.resolvedPath == nil) || (loader.ledger == nil && loader.report == nil)
		Button {
			Task {
				await loader.reload()
			}
		} label: {
			Image(systemName: "arrow.counterclockwise")
		}
		.buttonStyle(.borderless)
		.foregroundColor(buttonIsDisabled ? Color.gray : .accentColor)
		.help(tr("options.base_report.loader.reload.help"))
		.disabled(buttonIsDisabled)
	}

	private var _filePath : Binding<String>
	{
		Binding<String>(
			get: { loader.path ?? "" },
			set: { loader.path = $0 }
		)
	}
}

struct BaseDocumentLoaderView_Previews : PreviewProvider
{
	static var previews : some View
	{
		VStack
		{
			Wrapper()
			Divider()
			Wrapper(showLocation: true)
		}
		.padding()
	}

	struct Wrapper : View
	{
		@State var emptyLocation : String?
		@State var exampleLocation : String? = "/Users/crusty/Documents/report2021.bhreport"
		let showLoc : Bool

		init(showLocation : Bool = false)
		{
			self.showLoc = showLocation
		}

		var body : some View
		{
			BaseDocumentLoaderView(loader: BaseDocumentLoader())
		}
	}
}

// MARK: -

#if canImport(AppKit)
import AppKit
#elseif canImport(UIKit)
import UIKit
#endif
import UniformTypeIdentifiers

struct FileOpenButton : View
{
	var fileTypes : [UTType] = []
	let action : (URL?) -> Void

	var body : some View
	{
		Button {
		#if canImport(AppKit)
			let openPanel = NSOpenPanel()
			openPanel.canChooseDirectories = false
			openPanel.canChooseFiles = true
			openPanel.canCreateDirectories = false
			openPanel.allowsMultipleSelection = false
			openPanel.allowedContentTypes = fileTypes
			let result = openPanel.runModal()
			action(result == .OK ? openPanel.url : nil)
		#elseif canImport(UIKit)
			logger.debug("Loading base documents from files is not supported on iOS.")
		#endif
		} label: {
			Image(systemName: "folder")
		}
		.buttonStyle(.borderless)
	}
}

struct FileOpenButton_Previews : PreviewProvider
{
	static var previews : some View
	{
		FileOpenButton { _ in
		}
		.padding()
	}
}
