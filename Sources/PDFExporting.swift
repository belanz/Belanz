// Copyright 2020-2022 Kai Oezer

#if canImport(UIKit)
import UIKit

import Buchhaltung
import PDFKit

/// https://developer.apple.com/documentation/quartz/pdfkit
/// https://developer.apple.com/videos/play/wwdc2017/241/
/// https://www.raywenderlich.com/4023941-creating-a-pdf-in-swift-with-pdfkit

extension Assets
{
	func exportPDF(to fileLocation : URL) throws
	{
		let pageFormat = UIGraphicsPDFRendererFormat()
		pageFormat.documentInfo = [
			kCGPDFContextCreator: "Belanz",
			kCGPDFContextAuthor: "<company name>",
			kCGPDFContextTitle: "Inventar"
			// for all keys see https://developer.apple.com/documentation/coregraphics/cgcontext/auxiliary_dictionary_keys
		] as [String:Any]

		let A4PaperSize = CGSize(width:8.3 * 72.0, height: 11.7 * 72.0) // the PDF standard uses 72 points per inch
		let pageRect = CGRect(origin: .zero, size: A4PaperSize)
		let renderer = UIGraphicsPDFRenderer(bounds: pageRect, format: pageFormat)

		try renderer.writePDF(to: fileLocation) { context in
			context.beginPage()

			// Render the page here using the Core Graphics API.
			var vOffset : CGFloat = 0.0 // current drawing descend from the top of the page
			_addSpacing(&vOffset)
			_drawCompanyHeader(verticalOffset: &vOffset)
			_addSpacing(&vOffset)
			_drawTitle(in: pageRect, verticalOffset: &vOffset)
			_addSpacing(&vOffset)
		}
	}

	private func _drawCompanyHeader(verticalOffset : inout CGFloat)
	{

	}

	private func _drawTitle(in rect : CGRect, verticalOffset : inout CGFloat)
	{
		let titleAttributes : [NSAttributedString.Key: Any] = [
			NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0, weight: .bold)
		]
		let textHeight = _drawCenteredText("Inventar zum 31.12.2020", attributes: titleAttributes, in: CGRect(x: rect.minY, y: verticalOffset, width: rect.width, height: rect.height))
		verticalOffset += textHeight
	}

	/// - returns: the height of the drawn text
	private func _drawCenteredText(_ text : String, attributes : [NSAttributedString.Key : Any], in rect : CGRect) -> CGFloat
	{
		let attributedText = NSAttributedString(string: text, attributes: attributes)
		let textSize = attributedText.size()
		let textRect = CGRect(x: (rect.width - textSize.width) / 2.0, y: rect.minY, width: textSize.width, height: textSize.height)
		attributedText.draw(in: textRect)
		return textSize.height
	}

	/// - returns: the height of the drawn text block
	private func _drawWrappingText(_ text : String, attributes : [NSAttributedString.Key : Any], in rect : CGRect) -> CGFloat
	{
		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.alignment = .natural
		paragraphStyle.lineBreakMode = .byWordWrapping

		let textAttributes : [NSAttributedString.Key : Any] = attributes.merging(
			[NSAttributedString.Key.paragraphStyle : paragraphStyle], uniquingKeysWith: { $1 })
		let attributedText = NSAttributedString(string: text, attributes: textAttributes)
		attributedText.draw(in: rect)
		return rect.height
	}

	/// - returns: the height of the drawn image
	private func _drawImage(_ image : UIImage, in rect : CGRect, preservingAspectRatio : Bool = true) -> CGFloat
	{
		if preservingAspectRatio
		{
			image.draw(in: rect)
			return rect.size.height
		}
		else
		{
			let aspectRatio = min(rect.width / image.size.width, rect.height / image.size.height)
			let width = image.size.width * aspectRatio
			let height = image.size.height * aspectRatio
			image.draw(in: CGRect(x: (rect.width - width) / 2.0, y: rect.minY, width: width, height: height))
			return height
		}
	}

	private func _addSpacing(_ offset : inout CGFloat)
	{
		offset += 16.0
	}
}

#endif
