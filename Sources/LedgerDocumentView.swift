// Copyright 2023 Kai Oezer

import SwiftUI
import BuchhaltungUI
import Buchhaltung

struct LedgerDocumentView : View
{
	@Binding var ledger : Ledger
	@StateObject var buchhaltungCommands = BuchhaltungCommands()
	@Environment(\.bhHighlightBackgroundColor) var highlightBackgroundColor : Color

	var body : some View
	{
		let ui = BuchhaltungViews(ledger: $ledger)
		VStack(spacing: 0)
		{
			HStack
			{
				ui.optionsBar
				Spacer()
			}
			.padding([.leading, .trailing], 16)
			.padding([.top, .bottom], 10)
			.background(highlightBackgroundColor)

			SectionPicker(sections: BelanzDocumentSection.ledgerSections) {
				ui.companyInfoView
				ui.inventoryView()
				ui.journalView
			}
		}
		.background(Color("document_background"))
		.focusedSceneObject(buchhaltungCommands)
	#if os(macOS)
		.presentedWindowToolbarStyle(.unifiedCompact(showsTitle: true))
		.presentedWindowStyle(.titleBar)
	#else
		.navigationBarTitleDisplayMode(.inline)
	#endif
	}

}
