//  Copyright 2020-2023 Kai Oezer

import Foundation
import CodableCSV

/**
	Represents an entry in an asset depreciation table.

	The data from the tables is used to present the user a list of items with known depreciation durations.
	The depreciation (in years) is then used to set the `depreciationYears` property of
	`Buchhaltung.FixedAsset` or `Buchhaltung.CurrentAsset`

	German: AfA-Tabelle (Absetzung für Abnutzung)

	Die begrenzte Nutzungsdauer wird üblicherweise aus den
	[vom Finanzamt veröffentlichten](https://www.bundesfinanzministerium.de/Web/DE/Themen/Steuern/Steuerverwaltungu-Steuerrecht/Betriebspruefung/AfA_Tabellen/afa_tabellen.html)
	**AfA-Tabellen** ("Absetzung für Abnutzung") entnommen. Es gibt branchenspezifische Tabellen
	und eine Tabelle für *all­ge­mein ver­wend­ba­re An­la­ge­gü­ter (AV)*.
	Die Tabellen geben die Nutzungsdauer in Jahren an. Die Absetzung erfolgt linear in jedem
	vollen Kalendermonat bis die Nutzungsdauer erreicht und der Wert auf Null gesunken ist.

	Die Nutzungsdauer von **Computerhardware und Software zur Dateneingabe und -verarbeitung** wurde
	im Februar 2021 per [BMF-Schreiben](https://www.bundesfinanzministerium.de/Content/DE/Downloads/BMF_Schreiben/Steuerarten/Einkommensteuer/2021-02-26-nutzungsdauer-von-computerhardware-und-software-zur-dateneingabe-und-verarbeitung.html)
	von 3 Jahren auf 1 Jahr herabgesetzt. Computer und Software sind somit sofort abschreibbar.
*/
public struct AfA : Codable
{
	struct Entry : Codable, Hashable, Comparable
	{
		let section : String
		let assetType : String
		let depreciationYears : UInt8?

		func hash(into hasher: inout Hasher)
		{
			hasher.combine(assetType)
		}

		static func < (lhs: AfA.Entry, rhs: AfA.Entry) -> Bool
		{
			lhs.assetType < rhs.assetType
		}

		// swiftlint:disable nesting
		enum CodingKeys : String, CodingKey
		{
			case section = "Fundstelle"
			case assetType = "Anlagegueter"
			case depreciationYears = "Nutzungsdauer (Jahre)"
		}
		// swiftlint:enable nesting
	}

	let entries : Set<Entry>

	init()
	{
		entries = Set<Entry>()
	}

	init?(from archiveLocation : URL)
	{
		let decoder = CSVDecoder {
			$0.delimiters.field = ";"
			$0.delimiters.row = "\r\n"
			$0.headerStrategy = .firstLine
			$0.encoding = .utf8
			$0.nilStrategy = .empty
			$0.presample = false
			$0.decimalStrategy = .locale(belanzLocale)
			$0.dataStrategy = .base64
			$0.bufferingStrategy = .sequential
			$0.trimStrategy = .whitespaces
		}
		guard let tableEntries = try? decoder.decode([Entry].self, from: archiveLocation) else { return nil }
		entries = Set(tableEntries)
	}
}
