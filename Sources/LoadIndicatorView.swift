// Copyright 2023 Kai Oezer

import SwiftUI
import Combine

struct LoaderIndicatorView<V> : View where V : View
{
	var loadedView : V
	var loader : () async -> Bool

	@State var isLoaded : Bool = false
	@State var showsSpinner : Bool = false

	// timer for delaying the appearance of the wait indicator a little
	var _waitIndicatorTimerPublisher = Timer.publish(every: 0.3, on: .main, in: .common)
	@State var _timerConnection : Cancellable?

	var body: some View
	{
		if isLoaded
		{
			loadedView
		}
		else
		{
			_waitIndicator
				.task(priority: .userInitiated) {
					isLoaded = await loader()
				}
		}
	}

	private var _waitIndicator : some View
	{
		GeometryReader { geo in
			VStack
			{
				Spacer()
					.frame(height: 0.20 * geo.size.height)

				VStack(spacing: 8)
				{
					if showsSpinner
					{
						ProgressView()
							.frame(height: 60)
						Text("resource_loading.wait_indicator")
					}
				}
				.frame(width: 300, height: 100)
				.onAppear {
					_timerConnection = _waitIndicatorTimerPublisher.connect()
				}
				.onReceive(_waitIndicatorTimerPublisher) { _ in
					showsSpinner = true
					_timerConnection?.cancel()
				}

				Spacer()
			}
			.frame(maxWidth: .infinity, maxHeight: .infinity)
			.background(Color.white)
		}
	}

}
