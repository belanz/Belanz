// Copyright 2020-2024 Kai Oezer

import SwiftUI
import Buchhaltung
import BuchhaltungUI
import IssueCollection

struct BelanzDocumentView: View
{
	@Binding var document : BelanzDocument
	let documentLocation : URL?
	@State var reportType : ReportType = .fiscal
	@State var showsBaseReport : Bool = false
	@StateObject var buchhaltungCommands = BuchhaltungCommands()
	@StateObject var baseDocumentLoader = BaseDocumentLoader()
	@Environment(\.bhHighlightBackgroundColor) var highlightBackgroundColor : Color
	@Environment(\.openDocument) private var openDocumentAction
	@EnvironmentObject var degaap : ObservableDEGAAPEnvironment

	var body : some View
	{
		VStack(spacing: 0)
		{
			let ui = BuchhaltungViews(ledger: $document.ledger, reportType: $reportType)

			HStack
			{
				ui.optionsBar
					.disabled(showsBaseReport)
				_baseReportLoader
			}
			.padding([.leading, .trailing], 16)
			.padding([.top, .bottom], 10)
			.background(highlightBackgroundColor)

			if showsBaseReport {
				ui.baseReportView
					.background(highlightBackgroundColor)
					.transition(.opacity)
					.toolbar {
						_baseDocumentOpenButton
					}
			} else {
				SectionPicker(sections: BelanzDocumentSection.allSections) {
					ui.companyInfoView
					ui.inventoryView(baseAssets: baseDocumentLoader.ledger?.assets, baseDebts: baseDocumentLoader.ledger?.debts)
					ui.journalView
					ui.reportGeneratorView(
						report: document.report,
						timestamp: document.reportTimestamp,
						generator: BuchhaltungViews.ReportGenerator(generate: {
							var issues = IssueCollection()
							let newReport = try? await document.ledger.generateReport(type: reportType, withInventory: true, using: degaap.degaap, issues: &issues)
							Task { @MainActor in
								withAnimation {
									document.report = newReport
									document.reportTimestamp = .now
									document.issues = EmbeddableIssueCollection(issues: issues)
								}
							}
						}, clear: {
							withAnimation {
								document.report = nil
								document.reportTimestamp = nil
								document.issues.clear()
							}
						}))
						.modifier(ToolbarFileExporter(
							item: document.report,
							fileBaseName: document.report?.title.description ?? "report",
							disabled: (document.report == nil)
						))
						.environmentObject(ObservableEmbeddableIssueCollection(issues: document.issues))
				}
			}
		}
		.background(Color("document_background"))
		.focusedSceneObject(buchhaltungCommands)
	#if os(macOS)
		.presentedWindowToolbarStyle(.unifiedCompact(showsTitle: true))
		.presentedWindowStyle(.titleBar)
	#else
		.navigationBarTitleDisplayMode(.inline)
	#endif
		.animation(.spring(dampingFraction: 1.0), value: showsBaseReport)
		.onChange(of: baseDocumentLoader.report, initial: false) { _, newReport in
			document.ledger.baseReport = newReport
		}
		.onChange(of: baseDocumentLoader.path, initial: false) { _, newPath in
			document.baseReportLocation = newPath
		}
		.task {
			baseDocumentLoader.documentLocation = documentLocation
			baseDocumentLoader.path = document.baseReportLocation
		}
	}

	private var _baseReportLoader : some View
	{
		HStack
		{
			ViewThatFits(in: .horizontal)
			{
				txt("options.base_report.label")
					.lineLimit(1)
					.opacity(0.5)
				Spacer()
					.frame(width: 0)
			}

			BaseDocumentLoaderView(loader: baseDocumentLoader)
			.disabled(showsBaseReport)
			.frame(minWidth: 150)

			_baseReportViewingButton
		}
	}

	private var _baseReportViewingButton : some View
	{
		Button {
			withAnimation(.spring(dampingFraction: 1.0, blendDuration: 0.5)) {
				showsBaseReport.toggle()
			}
		} label: {
			Image(systemName: showsBaseReport ? "eye.fill" : "eye")
				.foregroundColor(document.ledger.baseReport == nil ? Color.gray : .accentColor)
		}
		.buttonStyle(.borderless)
		.help(showsBaseReport ? "options.base_report.viewing.close.help" : "options.base_report.viewing.show.help")
		.disabled(document.ledger.baseReport == nil)
	}

	private var _baseDocumentOpenButton : some View
	{
		Button {
			if let baseReportPath = baseDocumentLoader.resolvedPath {
				let baseReportLocation = URL(fileURLWithPath: baseReportPath)
				Task { @MainActor in
					do {
						try await openDocumentAction(at: baseReportLocation)
					} catch let error {
						logger.error("Could not open the document at \(baseReportPath). \(error.localizedDescription)")
					}
				}
			}
		} label: {
			Image(systemName: "macwindow")
		}
		.help("options.base_report.viewing.toolbar.open")
	}
}

// MARK: -

struct BelanzDocumentView_Previews: PreviewProvider
{
	static var previews: some View
	{
		Group
		{
			_config("2022 Company Info", .companyInfo)
			_config("2022 Assets and Debts", .inventory)
			_config("2022 Journal", .journal)
			_config("2022 Report", .report)
			_config("2023 Company Info", .companyInfo)
		}
		.frame(width: 800, height: 700)
		.environment(\.locale, .init(identifier: "de"))
		.previewDevice("Mac")
	}

	@ViewBuilder
	private static func _config(_ title : String, _ section : BelanzDocumentSection) -> some View
	{
		let doc = title.starts(with: "2022 ") ? _document(.y2022) : _document(.y2023)
		VStack {
			Wrapper(document: doc.0, documentLocation: doc.1, section: section)
		}
		.previewDisplayName(title)
	}

	private static var _cachedDocuments = [DocYear : (BelanzDocument, URL?)]()

	private enum DocYear : Int
	{
		case y2022 = 2022
		case y2023 = 2023
	}

	private static func _document(_ year : DocYear) -> (BelanzDocument, URL?)
	{
		if _cachedDocuments[year] == nil {
			if let documentAndLocation = _loadDocument("Gheyst\(year.rawValue)") {
				_cachedDocuments[year] = documentAndLocation
			} else {
				_cachedDocuments[year] = (BelanzDocument(), nil)
			}
		}
		return _cachedDocuments[year]!
	}

	private static func _loadDocument(_ fileName : String) -> (BelanzDocument, URL?)?
	{
		guard let fileLocation = Bundle.main.url(forResource: fileName, withExtension: "belanz"),
			let fileContents = try? Data(contentsOf: fileLocation),
			let document = try? BelanzDocument(fileContents: fileContents, type: .belanz) else {
			return nil
		}
		return (document, fileLocation)
	}

	struct Wrapper : View
	{
		@State var document : BelanzDocument = BelanzDocument()
		var documentLocation : URL?
		@State var section : BelanzDocumentSection = .companyInfo
		@StateObject var loader = BaseDocumentLoader()

		var body : some View
		{
			BelanzDocumentView(
				document: self.$document,
				documentLocation: documentLocation
			)
		}
	}
}
