// Copyright 2020-2025 Kai Oezer

import SwiftUI
import os

let logger = os.Logger(subsystem: "belanz.belanz", category: "Belanz")

// MARK: localization functions

func txt(_ loc : LocalizedStringKey) -> Text
{
	Text(loc, bundle: .main)
}

/// - returns: the localized string for the given localization value
func tr(_ loc : StaticString) -> String
{
	String(localized: LocalizedStringResource(loc, defaultValue: .init("!loc!"), table: "Localizable", locale: .autoupdatingCurrent, bundle: .main, comment: ""))
}

/// - returns: the localized string for the given localization value
func tr(_ loc : String) -> String
{
#if true
	NSLocalizedString(loc, tableName: "Localizable", bundle: .main, value: "!\(loc)!", comment: "") as String
#else
	String(localized: LocalizedStringResource(.init(loc), table: "Localizable", locale: .autoupdatingCurrent, bundle: .main, comment: ""))
#endif
}

let belanzLocale = Locale(identifier: "de_DE")

struct Utils
{
	static func relativePath(from sourceURL : URL, to targetURL : URL) -> String?
	{
		let sourceComps = sourceURL.standardized.absoluteURL.pathComponents
		let targetComps = targetURL.standardized.absoluteURL.pathComponents
		let sharedPathLength = zip(sourceComps, targetComps).reduce(0, { $0 + (($1.0 == $1.1) ? 1 : 0) })
		let stepsFromSourceToSharedDir = sourceComps.count - sharedPathLength - 1
		let relativePath = [String](repeating: "..", count: stepsFromSourceToSharedDir) + targetComps[sharedPathLength...]
		return relativePath.joined(separator: "/")
	}
}
