// Copyright 2020-2025 Kai Oezer

import SwiftUI
import UniformTypeIdentifiers
import Buchhaltung
import TOMLike
import IssueCollection

struct BelanzDocument
{
	var ledger : Ledger

	/// the string representation of a URL which points to the BH-encoded base report
	var baseReportLocation : String?

	var reportType : ReportType = .fiscal
	var report : Report?
	var reportTimestamp : Date?

	var issues = EmbeddableIssueCollection()

	static let fileExtension = "belanz"

	init()
	{
		self.ledger = Ledger(period: .currentYear!, company: Company())
	}

	init(fileContents : Data, type : UTType) throws
	{
		guard type == .belanz else {
			throw BelanzError.contentTypeNotSupported
		}
		guard let archivedDocument = String(data: fileContents, encoding: .utf8) else {
			throw BelanzError.documentUnarchiving
		}
		var decodingIssues = IssueCollection()
		let document = try TOMLikeCoder.decode(Self.self, from: archivedDocument, issues: &decodingIssues)
		assert(decodingIssues.issues.isEmpty)
		if !decodingIssues.issues.isEmpty { logger.error("Document decoding issues: \(decodingIssues)") }
		self.init(document)
		self.issues.append(decodingIssues)
	}

	private init(_ other : BelanzDocument)
	{
		self.ledger = other.ledger
		self.baseReportLocation = other.baseReportLocation
		self.report = other.report
		self.reportTimestamp = other.reportTimestamp
		self.reportType = other.reportType
		self.issues = other.issues
	}

	func archive() throws -> Data
	{
		let archive = try TOMLikeCoder.encode(self)
		guard let fileData = archive.data(using: .utf8) else {
			throw BelanzError.documentArchivingDataConversion
		}
		return fileData
	}
}

extension BelanzDocument : FileDocument
{
	static var readableContentTypes: [UTType] { [.belanz] }
	static var writableContentTypes: [UTType] { [.belanz] }

	init(configuration: ReadConfiguration) throws
	{
		guard let fileContents = configuration.file.regularFileContents else {
			throw CocoaError(.fileReadCorruptFile)
		}
		try self.init(fileContents: fileContents, type: configuration.contentType)
	}

	func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper
	{
		guard configuration.contentType == .belanz else { throw BelanzError.contentTypeNotSupported }
		return try .init(regularFileWithContents: archive())
	}
}

extension BelanzDocument : Codable
{
	enum CodingFormat : Int, Codable
	{
		case v1 = 1
	}

	enum DocumentCodingKeys : String, CodingKey
	{
		case format = "format"
		case ledger = "ledger"
		case baseReportLocation = "base_report_location"
		case reportType = "report_type"
		case reportTimestamp = "report_timestamp"
		case report = "report"
		case documentIssues = "issues"
	}

	init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: DocumentCodingKeys.self)
		let format = try container.decode(Int.self, forKey: .format)
		guard format == 1 else { throw BelanzError.documentFormatNotSupported }
		ledger = try container.decode(Ledger.self, forKey: .ledger)
		baseReportLocation = try container.decodeIfPresent(String.self, forKey: .baseReportLocation)
		reportType = try container.decode(ReportType.self, forKey: .reportType)
		reportTimestamp = try container.decodeIfPresent(Date.self, forKey: .reportTimestamp)
		report = try container.decodeIfPresent(Report.self, forKey: .report)
		issues = (try? container.decodeIfPresent(EmbeddableIssueCollection.self, forKey: .documentIssues)) ?? EmbeddableIssueCollection()
	}

	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: DocumentCodingKeys.self)
		try container.encode(CodingFormat.v1.rawValue, forKey: .format)
		try container.encode(ledger, forKey: .ledger)
		try container.encodeIfPresent(baseReportLocation, forKey: .baseReportLocation)
		try container.encode(reportType, forKey: .reportType)
		try container.encodeIfPresent(reportTimestamp, forKey: .reportTimestamp)
		try container.encodeIfPresent(report, forKey: .report)
		if !issues.isEmpty {
			try container.encode(issues, forKey: .documentIssues)
		}
	}
}
