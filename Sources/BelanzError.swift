// Copyright 2023 Kai Oezer

enum BelanzError : Error
{
	case documentUnarchiving
	case documentArchivingDataConversion
	case documentFormatNotSupported
	case contentTypeNotSupported
	case fileExport
}
