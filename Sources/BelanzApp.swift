// Copyright 2020-2024 Kai Oezer

import SwiftUI
import DEGAAP
import BuchhaltungUI

@main
struct BelanzApp: App
{
	let degaap = DEGAAPEnvironment()

	var body: some Scene
	{
		Group
		{
			DocumentGroup(newDocument: BelanzDocument()) { config in
				BelanzDocumentView(document: config.$document, documentLocation: config.fileURL)
					.environment(\.bhErrorColor, Color("error"))
					.environment(\.bhFocusedColor, Color("focused"))
					.environment(\.bhHighlightBackgroundColor, Color("highlight_background"))
					.environmentObject(ObservableDEGAAPEnvironment(degaap: degaap))
			}
			.commands{ BelanzDocumentCommands() }

			DocumentGroup(viewing: LedgerDocument.self) { config in
				LedgerDocumentView(ledger: config.$document.ledger)
					.environment(\.bhErrorColor, Color("error"))
					.environment(\.bhFocusedColor, Color("focused"))
					.environment(\.bhHighlightBackgroundColor, Color("highlight_background"))
					.environmentObject(ObservableDEGAAPEnvironment(degaap: degaap))
			}

			DocumentGroup(viewing: ReportDocument.self) { config in
				ReportDocumentView(report: config.document.report)
					.environment(\.bhHighlightBackgroundColor, Color("highlight_background"))
					.environmentObject(ObservableDEGAAPEnvironment(degaap: degaap))
			}
		}
	#if os(macOS)
		.defaultSize(width: 900, height: 700)
		.windowResizability(.contentSize)
	#endif

	#if os(macOS)
		Settings
		{
			SettingsView()
		}
	#endif
	}
}
