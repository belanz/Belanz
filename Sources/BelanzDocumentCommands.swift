//  Copyright 2022-2023 Kai Oezer

import Foundation
import SwiftUI
import BuchhaltungUI

struct BelanzDocumentCommands : Commands
{
	@FocusedObject var sectionPickerInfo : SectionPickerInfo?
	@FocusedObject var buchhaltungCommands : BuchhaltungCommands?

	// For the commands layout and activation state to adapt to the
	// view focus state as expected by the architects of SwiftUI,
	// ALL COMMANDS NEED TO BE ADDED HERE FROM THE BEGINNING.
	// Only the enabled state of the commands should change,
	// depending on the focused view.
	var body : some Commands
	{
		CommandGroup(after: .toolbar)
		{
			_sectionPickerCommands
			Divider()
		}

		CommandMenu(LocalizedStringKey("command.menu.journal"))
		{
			_journalCommands
		}

		_helpCommands
	}

	@ViewBuilder
	private var _sectionPickerCommands : some View
	{
		ForEach(BelanzDocumentSection.allSections) { section in
			if let shortcutCharacter = "\(section.rawValue)".first {
				self._sectionSwitchButton(section, KeyEquivalent(shortcutCharacter))
					.disabled(!(sectionPickerInfo?.sections.contains(section) ?? false))
			}
		}
	}

	private func _sectionSwitchButton(_ targetSection : BelanzDocumentSection, _ shortcut : KeyEquivalent) -> some View
	{
		Button {
			sectionPickerInfo?.selectedSection?.wrappedValue = targetSection
		} label: {
			Label {
				Text(targetSection.description)
			} icon: {
				targetSection.icon
			}
		}
		.keyboardShortcut(shortcut, modifiers: .command)
	}

	@ViewBuilder
	private var _journalCommands : some View
	{
		Button {
			buchhaltungCommands?.addJournalRecord?()
		} label: {
			Text("command.journal.add_record")
		}
		.keyboardShortcut("B", modifiers: .command, localization: .custom)
		.disabled(buchhaltungCommands?.addJournalRecord == nil)

		Button {
			buchhaltungCommands?.duplicateJournalRecord?()
		} label: {
			Text("command.journal.duplicate_record")
		}
		.keyboardShortcut("D", modifiers: .command, localization: .custom)
		.disabled(buchhaltungCommands?.duplicateJournalRecord == nil)
	}

	private var _helpCommands : some Commands
	{
		CommandGroup(replacing: .help)
		{
			Link("link.user_guide.title", destination: URL(string: String(localized:"link.user_guide.target"))!)
		}
	}
}
