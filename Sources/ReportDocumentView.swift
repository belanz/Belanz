// Copyright 2023 Kai Oezer

import SwiftUI
import BuchhaltungUI
import Buchhaltung

struct ReportDocumentView : View
{
	let report : Buchhaltung.Report
	@Environment(\.bhHighlightBackgroundColor) var highlightBackgroundColor : Color

	var body : some View
	{
		BuchhaltungViews.reportView(report: report)
			.background(highlightBackgroundColor)
	}
}
