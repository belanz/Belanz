// Copyright 2023-2024 Kai Oezer

import CoreTransferable
import Buchhaltung
import TOMLike
import IssueCollection
import UniformTypeIdentifiers
#if FEATURE_MYEBILANZ
import AppleArchive
import System
#endif

extension Report : @retroactive Transferable
{
	public static var transferRepresentation : some TransferRepresentation
	{
		FileRepresentation(contentType: .buchhaltungReport) { report in
			let fm = FileManager.default
			let fileLocation = fm.temporaryDirectory.appendingPathComponent("\(report.title).bhreport")
			try TOMLikeCoder.encode(report).write(to: fileLocation, atomically: true, encoding: .utf8)
			return SentTransferredFile(fileLocation, allowAccessingOriginalFile: false)
		} importing: { received in
			let fm = FileManager.default
			guard let receivedData = fm.contents(atPath: received.file.path),
				let archive = String(data: receivedData, encoding: .utf8)
				else { throw BelanzError.documentUnarchiving }
			var decodingIssues = IssueCollection()
			return try TOMLikeCoder.decode(Report.self, from: archive, issues: &decodingIssues)
		}

		DataRepresentation(exportedContentType: .buchhaltungReport) {
			try TOMLikeCoder.encode($0).data(using: .utf8) ?? Data()
		}

#if FEATURE_LATEX
		FileRepresentation(exportedContentType: .belanzLaTeXReport) { report in
			let latexRepresentation = try LaTeXEncoder.encode(report)
			let targetFileName = UUID().uuidString + ".tex"
			let fm = FileManager.default
			let targetLocation = fm.temporaryDirectory.appending(path:targetFileName, directoryHint: .notDirectory)
			assert(!fm.fileExists(atPath: targetLocation.path))
			try latexRepresentation.write(to: targetLocation, atomically: true, encoding: .utf8)
			return SentTransferredFile(targetLocation, allowAccessingOriginalFile: false)
		}
#endif

#if FEATURE_MYEBILANZ
		FileRepresentation(exportedContentType: .myebilanzReportArchive) { report in
			let (iniContents, csvContents) = try MyebilanzExporter.exportWithCSV(report: report)
			let fm = FileManager.default
			let workFolder = fm.temporaryDirectory.appending(path:UUID().uuidString, directoryHint: .isDirectory)
			try fm.createDirectory(at: workFolder, withIntermediateDirectories: false)
			let exportSourceFolder = workFolder.appending(path: report.title.description, directoryHint: .isDirectory)
			if fm.fileExists(atPath: exportSourceFolder.path) {
				try fm.removeItem(at: exportSourceFolder)
			}
			try fm.createDirectory(at: exportSourceFolder, withIntermediateDirectories: false)
			let fileBaseName = report.title.string.replacingOccurrences(of: " ", with: "_")

			let csvFileName = fileBaseName + "-Salden.csv"
			let csvFileLocation = exportSourceFolder.appending(path: csvFileName, directoryHint: .notDirectory)
			try csvContents.write(to: csvFileLocation, atomically: true, encoding: .utf8)

			let iniFileName = fileBaseName + ".ini"
			let iniContentsWithCSVFileName = iniContents.replacingOccurrences(of: "<csv file name>", with: csvFileName)
			let iniFileLocation = exportSourceFolder.appending(path: iniFileName, directoryHint: .notDirectory)
			try iniContentsWithCSVFileName.write(to: iniFileLocation, atomically: true, encoding: .utf8)

			let targetLocation = workFolder.appending(path: "\(report.title.description).aar", directoryHint: .notDirectory)
			try _archive(to: FilePath(targetLocation.path), from: FilePath(exportSourceFolder.path))
			return SentTransferredFile(targetLocation, allowAccessingOriginalFile: false)
		}

		FileRepresentation(exportedContentType: .myebilanzReport) { report in
			let iniContents = try MyebilanzExporter.export(report: report)
			let fm = FileManager.default
			let workFolder = fm.temporaryDirectory.appending(path:UUID().uuidString, directoryHint: .isDirectory)
			try fm.createDirectory(at: workFolder, withIntermediateDirectories: false)
			let fileName = report.title.string.replacingOccurrences(of: " ", with: "_") + ".ini"
			let targetLocation = workFolder.appending(path: fileName, directoryHint: .notDirectory)
			try iniContents.write(to: targetLocation, atomically: true, encoding: .utf8)
			return SentTransferredFile(targetLocation, allowAccessingOriginalFile: false)
		}
#endif
	}

#if FEATURE_MYEBILANZ
	/// Archives the given source directory into the given target file using AppleArchive.
	/// Requires that the target file is saved with the "aar" extension.
	private static func _archive(to target : FilePath, from sourceDirectory : FilePath) throws
	{
		guard let keySet = ArchiveHeader.FieldKeySet("TYP,PAT,LNK,DEV,DAT,UID,GID,MOD,FLG,MTM,BTM,CTM") else { throw BelanzError.fileExport }
		let targetPermissions : FilePermissions = [.ownerReadWrite, .groupRead, .otherRead]
		try ArchiveByteStream.withFileStream(path: target, mode: .writeOnly, options: [.create], permissions: targetPermissions) { fileOutputStream in
			try ArchiveByteStream.withCompressionStream(using: .lzfse, writingTo: fileOutputStream, blockSize: 200) { compressionStream in
				try ArchiveStream.withEncodeStream(writingTo: compressionStream) { encodeStream in
					try encodeStream.writeDirectoryContents(archiveFrom: sourceDirectory, keySet: keySet)
				}
			}
		}
	}
#endif
}
