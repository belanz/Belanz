// Copyright 2023 Kai Oezer

import Foundation
import SwiftUI
import CoreTransferable
import UniformTypeIdentifiers
#if canImport(AppKit)
import AppKit
#else
import UIKit
#endif
import Buchhaltung

@MainActor
struct ToolbarFileExporter : ViewModifier
{
	private let itemProvider : NSItemProvider
	private let isButtonDisabled : Bool
	private let targetFileBaseName : String

	private let exportableContentTypes : [UTType] = {
		var types = [UTType.buchhaltungReport]
#if FEATURE_LATEX
		types.append(contentsOf: [.belanzLaTeXReport])
#endif
#if FEATURE_MYEBILANZ
		types.append(contentsOf: [.myebilanzReport, .myebilanzReportArchive])
#endif
		return types
	}()

#if !canImport(AppKit)
	@State private var exportableFileLocation : URL?
#endif

	init<T>(item: T?, fileBaseName : String, disabled : Bool = false)
		where T : Transferable & Sendable
	{
		targetFileBaseName = fileBaseName
		isButtonDisabled = disabled
		itemProvider = NSItemProvider()
		if let item {
			itemProvider.register(item)
		}
	}

	func body(content : Content) -> some View
	{
		content
			.toolbar {
				ToolbarItemGroup(placement: .primaryAction)
				{
					Menu {
						ForEach(exportableContentTypes, id: \.identifier) { contentType in
							Button(LocalizedStringKey("export." + contentType.identifier)) {
								_export(contentType: contentType)
							}
						}
					} label: {
						Image(systemName: "square.and.arrow.up")
					}
					.disabled(isButtonDisabled)
					.help("export.report.help")
				}
			}
#if !canImport(AppKit)
			.sheet(isPresented: Binding(get: { exportableFileLocation != nil }, set: { if !$0 {exportableFileLocation = nil} })) {
				FileExporterSheetView(fileLocation : exportableFileLocation)
			}
#endif
	}

	private func _export(contentType : UTType)
	{
#if canImport(AppKit)
		let panel = NSSavePanel()
		panel.allowedContentTypes = [contentType]
		panel.allowsOtherFileTypes = false
		panel.isExtensionHidden = false
		panel.title = "export.report.panel_title"
		panel.nameFieldStringValue = targetFileBaseName // NSSavePanel appends the extension
		if panel.runModal() == .OK
		{
			if let targetLocation = panel.url {
				itemProvider.loadFileRepresentation(forTypeIdentifier: contentType.description) { sourceLocation, loadError in
					if let sourceLocation {
						do {
							let fm = FileManager.default
							if fm.fileExists(atPath: targetLocation.path) {
								try fm.removeItem(at: targetLocation)
							}
							try fm.moveItem(at: sourceLocation, to: targetLocation)
						} catch {
							logger.error("Could not copy file. \(error)")
						}
					} else {
						logger.error("\(loadError)")
					}
				}
			}
		}
#else
		itemProvider.loadFileRepresentation(forTypeIdentifier: contentType.description) { sourceLocation, loadError in
			if let sourceLocation {
				// The file at the provided location needs to be copied.
				// It will be deleted after this closure returns.
				let fm = FileManager.default
				let tmpLocation = fm.temporaryDirectory.appendingPathComponent(sourceLocation.lastPathComponent, isDirectory: false)
				do {
					if fm.fileExists(atPath: tmpLocation.path) {
						try fm.removeItem(at: tmpLocation)
					}
					try fm.moveItem(at: sourceLocation, to: tmpLocation)
					exportableFileLocation = tmpLocation
				}
				catch { logger.error("\(error)") }
			} else {
				logger.error("\(loadError)")
			}
		}
#endif
	}
}

#if !canImport(AppKit)

import UIKit

struct FileExporterSheetView : UIViewControllerRepresentable
{
	let fileLocation : URL!

	func makeUIViewController(context: Context) -> UIDocumentPickerViewController
	{
		assert(fileLocation != nil)
		let documentPicker = UIDocumentPickerViewController(forExporting: [fileLocation], asCopy: false)
		documentPicker.allowsMultipleSelection = false
		documentPicker.shouldShowFileExtensions = true
		return documentPicker
	}

	func updateUIViewController(_ uiViewController: UIDocumentPickerViewController, context: Context)
	{}
}

#endif
