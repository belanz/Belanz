// Copyright 2022-2024 Kai Oezer

import SwiftUI
import Buchhaltung
import TOMLike
import IssueCollection
import UniformTypeIdentifiers

@MainActor
class BaseDocumentLoader : ObservableObject
{
	@Published var ledger : Ledger?
	@Published var report : Report?
	@Published var path : String?
	{
		didSet {
			resolvedPath = _resolvePath()
			Task {
				await _load()
			}
		}
	}

	/// The resolved absolute path. Used when ``path`` is a relative path.
	private(set) var resolvedPath : String?

	/// The location of the currently open document.
	/// Required if the base document path is a relative path.
	var documentLocation : URL?
	{
		didSet {
			resolvedPath = _resolvePath()
			Task {
				await _load()
			}
		}
	}

	func reload() async
	{
		await _load()
	}

	/// - returns: The relative path from the document file to the given absolute file path.
	func documentRelativePath(to fileLocation : URL) -> String?
	{
		guard let documentLocation else { return nil }
		return Utils.relativePath(from: documentLocation, to: fileLocation)
	}

	private func _resolvePath() -> String?
	{
		guard let path else { return nil }
		let trimmedPath = path.trimmingCharacters(in: .whitespaces)
		guard !trimmedPath.isEmpty else { return nil }
		let fm = FileManager()
		var isDir : ObjCBool = false
		if fm.fileExists(atPath: trimmedPath, isDirectory: &isDir) && !isDir.boolValue { return trimmedPath }
		let isRelativePath = !trimmedPath.starts(with: "/")
		guard isRelativePath, let documentLocation else { return nil }
		let resolvedRelativePath = documentLocation.deletingLastPathComponent().appending(path: trimmedPath).path(percentEncoded: false)
		guard fm.fileExists(atPath: resolvedRelativePath, isDirectory: &isDir) && !isDir.boolValue else { return nil }
		return resolvedRelativePath
	}

	private func _load() async
	{
		guard let resolvedPath else {
			Task { @MainActor in
				ledger = nil
				report = nil
			}
			return
		}
		let fileLocation = URL(fileURLWithPath: resolvedPath)
		assert(fileLocation.scheme == "file")
		let pathExtension = fileLocation.pathExtension
		if pathExtension == UTType.belanz.preferredFilenameExtension {
			_loadBelanzDocument(from: fileLocation)
		} else if pathExtension == UTType.buchhaltungReport.preferredFilenameExtension {
			_loadBuchhaltungReport(from: fileLocation)
		}
	}

	private func _loadBelanzDocument(from fileLocation : URL)
	{
		guard let fileData = try? Data(contentsOf: fileLocation, options: .uncached),
			let document = try? BelanzDocument(fileContents: fileData, type: .belanz)
			else {
				Task { @MainActor in
					ledger = nil
					report = nil
				}
				return
			}
		Task { @MainActor in
			ledger = document.ledger
			report = document.report
		}
	}

	private func _loadBuchhaltungReport(from fileLocation : URL)
	{
		var decodingIssues = IssueCollection()
		guard let fileContents = try? String(contentsOf: fileLocation, encoding: .utf8),
			let report = try? TOMLikeCoder.decode(Report.self, from: fileContents, issues: &decodingIssues)
			else {
				Task { @MainActor in
					self.ledger = nil
					self.report = nil
				}
				return
			}
		Task { @MainActor in
			self.ledger = nil
			self.report = report
		}
	}
}
