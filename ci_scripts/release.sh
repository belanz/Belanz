#!/bin/sh


# Creates a release of the app with the given tag.
# The tag is used as the version number of the release.

function printUsage()
{
    echo "Usage:\n$0 <version string>"
}

if [[ ! $# -eq 1 ]]; then
  echo "A version string is required for the release"
  exit 1
fi

version="${1}"

#
# STAGE 1
# Building a the app and creating a distribution image.
# Do this before pushing the release tag to GitLab.
#
./build_belanz.sh $version
./create_disk_image.sh $version

#
# STAGE 2
# Creating a release on GitLab
#
git tag $version
git push --tags
# The distribution image generated in stage 1 should now
# be picked up by GitLab via the release-cli runner.

#
# STAGE 3
# Updating the website https://belanz.gitlab.io
#
cd ../../belanz.gitlab.io
git tag $version
git push --tags
# A GitLab Pages pipeline should now run and update the website.

