#!/bin/zsh

# Copyright 2023 Kai Oezer

# Replacing the normal app icon with the Beta app icon
# for tagged commits where the tag starts with "beta-".
if [[ $CI_TAG = "beta-"* && $CI_XCODEBUILD_ACTION = 'archive' ]]; then
  echo "Replacing Belanz app icon with Belanz Beta app icon."
  APP_ICON_PATH=$CI_WORKSPACE/Sources/Resources/Assets.xcassets/AppIcon.appiconset
  BETA_APP_ICON_PATH=$CI_WORKSPACE/ci_scripts/Assets/AppIcon-Beta.appiconset
  rm -rf $APP_ICON_PATH
  mv $BETA_APP_ICON_PATH $APP_ICON_PATH
fi
