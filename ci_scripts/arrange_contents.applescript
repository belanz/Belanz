(*
This script makes Finder arrange the items inside the Belanz distribution disk image.
The volume name needs to be supplied as argument when calling.

_____________________________________________________________________________
Adapted from 'template.applescript'
in https://github.com/create-dmg/create-dmg

Original copyright:

The MIT License (MIT)

Copyright (c) 2008-2014 Andrey Tarantsov
Copyright (c) 2020 Andrew Janke

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
_____________________________________________________________________________

*)
on run (volumeName)
	tell application "Finder"
		tell disk (volumeName as string)
			open
			
			set theXOrigin to 10
			set theYOrigin to 60
			set theWidth to 500
			set theHeight to 380
			
			set theBottomRightX to (theXOrigin + theWidth)
			set theBottomRightY to (theYOrigin + theHeight)
			set dsStore to "/Volumes/" & volumeName & "/.DS_Store"
			--log dsStore as string
			
			tell container window
				set current view to icon view
				set toolbar visible to false
				set statusbar visible to false
				set the bounds to {theXOrigin, theYOrigin, theBottomRightX, theBottomRightY}
			end tell
			
			set opts to the icon view options of container window
			tell opts
				set icon size to 96
				set text size to 14
				set arrangement to not arranged
			end tell
			set background picture of opts to file ".background:background.png"
			
			-- positioning items
			set position of item "Belanz.app" to {140, 80}
			set position of item "Programme" to {360, 80}
			set position of item "Notizen.pdf" to {250, 240}

			-- hiding file extension
			set extension hidden of item "Belanz.app" to true
			set extension hidden of item "Notizen.pdf" to true
		end tell
		
		-- giving the finder some time to write the .DS_Store file
		delay 2		
		--set waitTime to 0
		set ejectMe to false
		repeat while ejectMe is false
			delay 1
			--set waitTime to waitTime + 1
			--log waitTime
			set shellscript to "[ -f  \"" & dsStore & "\" ] && echo 0"
			--log shellscript
			if (do shell script shellscript) = "0" then set ejectMe to true
		end repeat
		--log "waited " & waitTime & " seconds for .DS_STORE to be created."
	end tell
end run
