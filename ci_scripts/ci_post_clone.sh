#!/bin/zsh
# Executed by Xcode Cloud after cloning.
# In this phase, we can access (and manipulate) all source files.

# Xcode Cloud provides an environment where Homebrew is available.
# Below is an example of installing a utility from Homebrew.

## code linting
#if [[ -n $CI_XCODEBUILD_ACTION = 'archive' ]]; then
#  if which brew > /dev/null; then
#    brew install swiftlint
#  fi
#  if which swiftlint > /dev/null; then
#    swiftlint ${CI_WORKSPACE}/Belanz | sed "s/error:/warning:/"
#  fi
#fi

